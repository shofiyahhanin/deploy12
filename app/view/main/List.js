/**
 * This view is an example list of people.
 */
Ext.define('Deploy12.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'Deploy12.store.Personnel'
    ],

    title: 'Data Mahasiswa',

    store: {
        type: 'personnel'
    },

    columns: [
        { text: 'Name',  dataIndex: 'name', flex : 1 },
        { text: 'Email', dataIndex: 'email', flex : 1 },
        { text: 'Phone', dataIndex: 'phone', flex: 1 }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
